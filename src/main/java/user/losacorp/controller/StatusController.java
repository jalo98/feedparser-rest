package user.losacorp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import user.losacorp.service.StartService;

/**
 * Created by Javier on 25/3/2017.
 */
@RestController
@RequestMapping("/")
public class StatusController {

    @Autowired
    private StartService startService;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status(Model model) {
        return "hello";
    }

    @RequestMapping(value = "/start", method = RequestMethod.GET)
    public String start() {
        return startService.run();
    }
}


