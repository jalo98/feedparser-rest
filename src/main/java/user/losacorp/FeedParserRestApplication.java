package user.losacorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedParserRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeedParserRestApplication.class, args);
	}
}
